import React from 'react';

function Home(){

    return(

    <div>
        <div id="main-carousel" className="carousel slide d-none d-sm-block" data-ride="carousel">
            <ol className="carousel-indicators">
            <li data-target="#main-carousel" data-slide-to="0" className="active"></li>
            <li data-target="#main-carousel" data-slide-to="1"></li>
            <li data-target="#main-carousel" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
                <div className="carousel-item">
                    <img className="d-block img-fluid" src="img/university-105709_1920.jpg" alt="First slide"/>
                </div>
                <div className="carousel-item active">
                    <img className="d-block img-fluid" src="img/people-2941977_1920.jpg" alt="Second slide"/>
                </div>
                <div className="carousel-item">
                    <img className="d-block img-fluid" src="img/student-2052868_1920.jpg" alt="Third slide"/>
                </div>
            </div>
            <a className="carousel-control-prev" href="#main-carousel" role="button" data-slide="prev" >
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
                <a className="carousel-control-next" href="#main-carousel" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
        </div>

        <div className="row row-content align-items-center">
            <div className="col col-sm order-sm-first col-md wow slideInRight">
                <div className="media">
                        <img height="300" width="370" className="d-flex mr-3 align-self-center" src="img/event-1597531_1920.jpg" alt="event"/>
                    <div className="media-body align-self-center ml-4">
                        <h2 className="mt-0">Pellentesque
                        </h2>
                        <p> 
                            Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div className="row row-content align-items-center">
            <div className="col col-sm order-sm-first col-md wow slideInLeft">
                <div className="media">
                    <div className="media-body align-self-center mr-4">
                        <h2 className="mt-0">Suspendisse
                        </h2>
                        <p> 
                            Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                        </p>
                    </div>
                    <img className="d-flex ml-3 align-self-center" src="img/paint-1280556_1920.jpg" alt="event"/>
                </div>
            </div>
        </div>

        <div className="row row-content align-items-center">
            <div className="col col-sm order-sm-first col-md wow slideInRight">
                <div className="media">
                        <img height="300" width="370" className="d-flex mr-3 align-self-center" src="img/online-3412473_1920.jpg" alt="event"/>
                    <div className="media-body align-self-center ml-4">
                        <h2 className="mt-0">Pellentesque
                        </h2>
                        <p> 
                            Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="services" className="container-fluid text-center row-content">
            <h2 className="mb-5 wow bounceInUp">SERVICES</h2>
            <h4 className="wow bounceInUp">What we offer</h4>
            <br/>
            <div className="row slideanim">
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-coffee fa-3x"></span>
                    <h4>POWER</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-heart fa-3x"></span>
                    <h4>LOVE</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-suitcase fa-3x"></span>
                    <h4>JOB DONE</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
            </div>
            <br/><br/>
            <div className="row slideanim">
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-filter fa-3x"></span>
                    <h4>OPTIONS</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-certificate fa-3x"></span>
                    <h4>CERTIFIED</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
                <div className="col-sm-4 wow bounceInUp">
                    <span className="fa fa-comments-o fa-3x"></span>
                    <h4 style={{color:'#303030'}}>OPORTUNITIES</h4>
                    <p className="fa-p">Lorem ipsum dolor sit amet..</p>
                </div>
            </div>
        </div>
    </div>
    );
}

export default Home;