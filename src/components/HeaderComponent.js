import React, {Component} from 'react';

import { Link } from 'react-router-dom';

import { Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input, Button} from 'reactstrap';

class Header extends Component{

    constructor(props, context) {
        super(props, context);
    
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    
        this.state = {
          show: false,
        };
      }
    
      toggleModal(){
        this.setState({
            show: !this.state.show
        });
    }

    handleLogin(){
        alert('Logged!');
    }

      render(){

        return(
        <>
            <nav className="navbar navbar-dark navbar-expand-sm fixed-top navbar-fixed-top ">
                <div className="container full-slider">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#Navbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <Link className="navbar-brand" to="/home"><img src="img/scive_13.png" alt="logo" height="50" width="80"/></Link>
                    <div className="collapse navbar-collapse" id="Navbar"> 
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item active"><Link className="nav-link" to="/home"><span className="fa fa-home fa-lg"></span> Home</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/about"><span className="fa fa-info fa-lg"></span> About</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/events"><span className="fa fa-calendar fa-lg"></span> Events</Link></li>
                            <li className="nav-item"><a className="nav-link" to="#"><span className="fa fa-address-card fa-lg"></span> Carrer</a></li>
                            <li className="nav-item"><a id="buttonLogin" className="nav-link" data-toggle="modal" data-target="#login-modal"><span className="fa fa-sign-in fa-lg fa-lgr"></span> login</a></li>
                            <li className="nav-item"><a className="nav-link pl-0" href=""><span className="fa fa-pencil fa-lg fa-lgr"></span><span className="reg-bar">|</span> register</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="login-modal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md" role="content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center-modal">Login</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            &times;
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="d-block mx-auto" src="./img/scive-logo-2.png" width="200" height="200"/>
                        <form>
                            <div class="form-row">
                                <div class="form-group col-12 input-group-lg">
                                        <label class="sr-only" for="email">Email address</label>
                                        <input type="email" class="form-control form-control-sm mr-1" id="exampleInputEmail3" placeholder="Enter email"/>
                                </div>
                                <div class="form-group col-12 input-group-lg">
                                    <label class="sr-only" for="password">Password</label>
                                    <input type="password" class="form-control form-control-sm mr-1" id="exampleInputPassword3" placeholder="Password"/>
                                </div>
                                <div class="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"/>
                                        <label class="form-check-label"> Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <button type="button" class="btn btn-secondary btn-sm ml-auto" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary btn-sm ml-1">Sign in</button>        
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </>
        );
}





}

export default Header;