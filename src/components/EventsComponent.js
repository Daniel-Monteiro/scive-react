import React from 'react';

function Events(){
    return(
        <div>
            <div className="jumbotron header-event">
                <div className="container">
                    <h3 className="mt-5 align-self-end"><u>Events</u></h3>
                </div>
            </div>

            <div className="container">
                <div className="row">

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Curabitur</h5>
                            </div>
                            <img className="card-img-top" src="img/places/barcelona-university-1174545.jpg" alt=""/>
                            <div className="card-body">
                                <p className="card-text">Ut ornare finibus libero, sit amet porttitor dolor dapibus eu. Nunc aliquet eu metus ac lacinia. Curabitur tempus nibh non cursus sagittis. Donec quis lorem sit amet urna hendrerit tristique. Nulla consequat sapien eros, nec ultrices libero volutpat et.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Vestibulum</h5>
                            </div>
                            <img className="card-img-top" src="img/places/building-in-university-1252614.jpg" alt=""/>
                            <div className="card-body">
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus arcu et ex dignissim dapibus. className aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mattis mauris a tortor ultrices viverra. Nam mattis iaculis ligula, vel pretium lectus euismod sed.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Maecenas</h5>
                            </div>
                            <img className="card-img-top" src="img/places/university-105709_1920.jpg" alt="Card image cap"/>
                            <div className="card-body">
                                <p className="card-text">Morbi sollicitudin auctor semper. Nullam id dapibus nunc. Etiam fringilla mi magna. Ut ornare finibus libero, sit amet porttitor dolor dapibus eu. Nunc aliquet eu metus ac lacinia. Curabitur tempus nibh non cursus sagittis.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row mt-4">

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Donec</h5>
                            </div>
                            <img className="card-img-top" src="img/places/state-university-of-new-york-1446991.jpg" alt="Card image cap"/>
                            <div className="card-body">
                                <p className="card-text">Duis in velit augue. Cras at aliquet leo, sit amet tristique eros. Aliquam tristique convallis tortor ut ornare. Mauris tempor dolor quis neque blandit, a facilisis dolor mollis. Etiam sodales consequat lectus eget gravida.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="container">
                            <div className="card wow fadeIn" style={{width: '18rem'}}>
                                <div className="card-header card-title-event">
                                    <h5 className="card-title">Sapien</h5>
                                </div>
                                <img className="card-img-top" src="img/places/university-105709_1920.jpg" alt="Card image cap"/>
                                <div className="card-body">
                                    <p className="card-text">Cras eu pellentesque mauris. Suspendisse et maximus tortor, ac luctus ante. Nam mollis orci nec purus varius porta. Mauris imperdiet vulputate cursus. Aenean at porta purus, vel congue nulla.</p>
                                    <a href="#" className="btn btn-primary">Register</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Quisque</h5>
                            </div>
                            <img className="card-img-top" src="img/places/university-in-wroclaw-1-1231133.jpg" alt="Card image cap"/>
                            <div className="card-body">
                                <p className="card-text">Phasellus nec libero non magna auctor volutpat non vel arcu. Donec mattis libero non turpis egestas, laoreet pellentesque purus volutpat. Etiam eget dui tempor, commodo tortor dictum, interdum justo. Quisque semper ligula sed nunc molestie, nec pretium odio luctus.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row mt-4">

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Curabitur</h5>
                            </div>
                                <img className="card-img-top" src="img/places/barcelona-university-1174545.jpg" alt="Card image cap"/>
                                <div className="card-body">
                                <p className="card-text">Ut ornare finibus libero, sit amet porttitor dolor dapibus eu. Nunc aliquet eu metus ac lacinia. Curabitur tempus nibh non cursus sagittis. Donec quis lorem sit amet urna hendrerit tristique. Nulla consequat sapien eros, nec ultrices libero volutpat et.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Vestibulum</h5>
                            </div>
                            <img className="card-img-top" src="img/places/building-in-university-1252614.jpg" alt="Card image cap"/>
                            <div className="card-body">
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus arcu et ex dignissim dapibus. className aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mattis mauris a tortor ultrices viverra. Nam mattis iaculis ligula, vel pretium lectus euismod sed.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="card wow fadeIn" style={{width: '18rem'}}>
                            <div className="card-header card-title-event">
                                <h5 className="card-title">Maecenas</h5>
                            </div>
                            <img className="card-img-top" src="img/places/university-105709_1920.jpg" alt="Card image cap"/>
                            <div className="card-body">
                                <p className="card-text">Morbi sollicitudin auctor semper. Nullam id dapibus nunc. Etiam fringilla mi magna. Ut ornare finibus libero, sit amet porttitor dolor dapibus eu. Nunc aliquet eu metus ac lacinia. Curabitur tempus nibh non cursus sagittis.</p>
                                <a href="#" className="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Events;