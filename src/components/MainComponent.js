import React, {Component} from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import  Header  from './HeaderComponent';
import  Footer  from './FooterComponent';
import Home from './HomeComponent';
import About from './AboutComponent';
import Events from './EventsComponent';

class Main extends Component {
    
    render(){

        return (
            <div>
                <Header />
                    <Switch >
                       <Route exact path='/home' component={Home} />
                       <Route exact path='/about' component={About}/>
                       <Route exact path='/events' component={Events}/>
                       <Redirect to="/home" />
                    </Switch>
                <Footer />
            </div>
        );
    }
}

export default Main;
