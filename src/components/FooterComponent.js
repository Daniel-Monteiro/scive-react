import React from 'react';
import { Link } from 'react-router-dom';

function Footer(){
    return(
        <footer className="footer about">
        <div className="container">
            <div className="row">             
                <div className="col-4 offset-1 col-sm-2 wow slideInUp">
                    <h5 className="f-title">Links</h5>
                    <ul className="list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="./aboutus.html">About</a></li>
                        <li><a href="./events.html">Events</a></li>
                        <li><a href="./carrer.html">Carrer</a></li>
                    </ul>
                </div>
                <div className="col-7 col-sm-5 wow slideInUp">
                    <h5 className="f-title">Our Address</h5>
                    <address>
                        99, Road to Imagination<br />
                        High Mountain, Who Knows<br />
                        Some Place in the World<br />
                        <i className="fa fa-phone fa-lg"></i> : +99 99999-9999<br />
                        <i className="fa fa-fax fa-lg"></i> : +99 99999-9999<br />
                        <i className="fa fa-envelope fa-lg"></i> : <a href="mailto:scive@events.net">scive@events.net</a>
                    </address>
                </div>
                <div className="col-12 col-sm-4 align-self-center">
                    <div className="text-center mb-2 wow lightSpeedIn">
                        <a className="btn btn-social-icon btn-google" href="http://google.com/+"><i className="fa fa-google-plus fa-lg"></i></a>
                        <a className="btn btn-social-icon btn-facebook" href="http://www.facebook.com/profile.php?id="><i className="fa fa-facebook fa-lg"></i></a>
                        <a className="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i className="fa fa-linkedin fa-lg"></i></a>
                        <a className="btn btn-social-icon btn-twitter" href="http://twitter.com/"><i className="fa fa-twitter fa-lg"></i></a>
                        <a className="btn btn-social-icon btn-youtube" href="http://youtube.com/"><i className="fa fa-youtube fa-lg"></i></a>
                        <a id="mail-icon" className="btn btn-social-icon" href="mailto:"><i className="fa fa-envelope-o fa-lg"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="right-footer" className="container-fluid">
            <div className="row justify-content-center">             
                <div className="col-auto">
                    <p className="mt-3">© Copyright 2019 Scive</p>
                </div>
            </div>
        </div>
    </footer>
    );
}

export default Footer;