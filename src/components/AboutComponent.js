import React from 'react';

function About(){
    return(
        <div>
            <header className="jumbotron about">
        <div className="container">
            <div className="row row-header">
                <div className="col-12 col-sm-8 align-self-center">
                    <h1><u>consectetur</u></h1>
                    <p>Proin id purus nec ipsum fringilla auctor quis at nisl. Sed vulputate iaculis dui et dapibus. Maecenas fringilla, neque eget vulputate congue, lorem leo consectetur orci, vitae aliquam nunc justo sit amet velit!</p>
                </div>
                <div className="col-12 col-sm align-self-center">
                    <img src="img/scive-logo-2.png" className="img-fluid" alt="logo"/>
                </div>
            </div>
        </div>
    </header>

    <div className="row row-content align-items-center">
        <div className="col col-sm order-sm-first col-md wow slideInRight">
            <div className="media">
                    <img height="300" width="370" className="d-flex mr-3 align-self-center" src="img/event-1597531_1920.jpg" alt="event"/>
                <div className="media-body align-self-center ml-4">
                    <h2 className="mt-0">Pellentesque
                    </h2>
                    <p> 
                        Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div className="row row-content align-items-center">
        <div className="col col-sm order-sm-first col-md wow slideInLeft">
            <div className="media">
                <div className="media-body align-self-center mr-4">
                    <h2 className="mt-0">Suspendisse
                    </h2>
                    <p> 
                        Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                    </p>
                </div>
                <img className="d-flex ml-3 align-self-center" src="img/paint-1280556_1920.jpg" alt="event"/>
            </div>
        </div>
    </div>

    <div className="row row-content align-items-center">
        <div className="col col-sm order-sm-first col-md wow slideInRight">
            <div className="media">
                    <img height="300" width="370" className="d-flex mr-3 align-self-center" src="img/online-3412473_1920.jpg" alt="event"/>
                <div className="media-body align-self-center ml-4">
                    <h2 className="mt-0">Pellentesque
                    </h2>
                    <p> 
                        Donec sollicitudin est vel velit congue, eu posuere nisi vestibulum. Duis rhoncus sem vel pretium blandit. Maecenas quis sem sit amet ex ullamcorper ornare sagittis tristique lacus. Aenean euismod quam vitae lacus laoreet pellentesque.
                    </p>
                </div>
            </div>
        </div>
    </div>
        </div>
    );
}

export default About;